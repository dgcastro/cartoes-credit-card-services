package br.com.cartao.cartaoservice.repository;

import br.com.cartao.cartaoservice.model.CreditCard;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
    Optional<CreditCard> findByNumber(String number);
}
