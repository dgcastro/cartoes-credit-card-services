package br.com.cartao.cartaoservice.service;

import br.com.cartao.cartaoservice.clients.CustomerClient;
import br.com.cartao.cartaoservice.model.CreditCard;
import br.com.cartao.cartaoservice.repository.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {
    @Autowired
    private CreditCardRepository creditCardRepository;
    @Autowired
    private CustomerClient customerClient;

    public CreditCard create(CreditCard creditCard) {
        //try {
            boolean existsById = customerClient.customerExistsById(creditCard.getCustomerId());
            //boolean existsById = true;
            if (existsById) {
                return creditCardRepository.save(creditCard);
            }else{
                throw new RuntimeException("Customer not found.");
            }
        //}catch (Exception exception){
           // throw new RuntimeException("Customer Service Error");
        //}
    }

    public CreditCard findByNumber(String number) {
        Optional<CreditCard> creditCardOptional = creditCardRepository.findByNumber(number);
        if(creditCardOptional.isPresent()){
            return creditCardOptional.get();
        }else{
            throw new RuntimeException("Credit Card not found.");
        }
    }

    public CreditCard activateCard(String number, boolean activate) {
        try{
            CreditCard creditCardActivated = findByNumber(number);
            creditCardActivated.setActive(true);
            return creditCardRepository.save(creditCardActivated);
        }catch (RuntimeException exception){
            throw new RuntimeException(exception.getMessage());
        }
    }
}
