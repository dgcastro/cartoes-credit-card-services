package br.com.cartao.cartaoservice.controller;

import br.com.cartao.cartaoservice.model.CreditCard;
import br.com.cartao.cartaoservice.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/creditcard")
public class CreditCardController {
    @Autowired
    private CreditCardService creditCardService;

    @PostMapping
    public CreditCard createCreditCard(@RequestBody CreditCard creditCard){
        try{
            return creditCardService.create(creditCard);
        }catch(Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PatchMapping("/{number}")
    public CreditCard activateCreditCard(@PathVariable String number){
        try{
            CreditCard creditCard = creditCardService.activateCard(number, true);
            return creditCard;
        }catch(Exception exception){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, exception.getMessage());
        }
    }

    @GetMapping("/{number}")
    public CreditCard findByNumber(@PathVariable String number){
        try{
            CreditCard creditCard = creditCardService.findByNumber(number);
            return creditCard;
        }catch (Exception exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
