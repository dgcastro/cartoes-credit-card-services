package br.com.cartao.cartaoservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class CartaoserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartaoserviceApplication.class, args);
	}

}
