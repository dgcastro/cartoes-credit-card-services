package br.com.cartao.cartaoservice.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomerClientDecoder implements ErrorDecoder {
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if(response.status() == 404){
            throw new RuntimeException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
