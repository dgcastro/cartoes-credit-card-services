package br.com.cartao.cartaoservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Customer")
public interface CustomerClient {

    @GetMapping("/customer/checkifexists/{id}")
    boolean customerExistsById(@PathVariable long id);
}
