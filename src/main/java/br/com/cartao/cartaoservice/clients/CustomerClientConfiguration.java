package br.com.cartao.cartaoservice.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CustomerClientConfiguration {
    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new CustomerClientDecoder();
    }
}
