package br.com.cartao.cartaoservice.service;

import br.com.cartao.cartaoservice.clients.CustomerClient;
import br.com.cartao.cartaoservice.model.CreditCard;
import br.com.cartao.cartaoservice.repository.CreditCardRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class CreditCardSerciceTest {
    @MockBean
    private CreditCardRepository creditCardRepository;

    @MockBean
    private CustomerClient customerClient;

    @Autowired
    private CreditCardService creditcardService;

    private CreditCard creditCard;

    @BeforeEach
    public void setUp(){
        creditCard = new CreditCard();
        creditCard.setCustomerId(1);
        creditCard.setActive(true);
        creditCard.setId(1);
        creditCard.setNumber("987654321");
    }

    @Test
    public void createCardTest(){
        Mockito.when(creditCardRepository.save(creditCard)).thenReturn(creditCard);
        Mockito.when(customerClient.customerExistsById(creditCard.getCustomerId())).thenReturn(true);
        creditCard.setActive(false);
        CreditCard creditCardCreateTest = creditcardService.create(creditCard);
        Assertions.assertEquals(creditCard, creditCardCreateTest);
        Assertions.assertEquals(creditCardCreateTest.isActive(), false);
    }

    @Test
    public void testarBuscarCartaoPorNumero(){
        Mockito.when(creditCardRepository.findByNumber("987654321")).thenReturn(Optional.of(creditCard));
        CreditCard creditCardByNumber = creditcardService.findByNumber("987654321");
        Assertions.assertEquals(creditCard, creditCardByNumber);
    }

    @Test
    public void testarBuscarCartaoPorNumeroInexistente(){
        Mockito.when(creditCardRepository.findByNumber(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {creditcardService.findByNumber("404");});
    }

    @Test
    public void testarAtivarCartao(){
        //creditCard.setActive(false);
        Mockito.when(creditCardRepository.findByNumber(Mockito.anyString())).thenReturn(Optional.of(creditCard));
        Mockito.when(creditCardRepository.save((creditCard))).thenReturn(creditCard);
        CreditCard creditCardActivate = creditcardService.activateCard("987654321", true);
        Assertions.assertEquals(true, creditCardActivate.isActive());
    }

    @Test
    public void testarAtivarCartaoInexistente(){
        Mockito.when(creditCardRepository.findByNumber(Mockito.anyString())).thenReturn(Optional.ofNullable(null));
        Assertions.assertThrows(RuntimeException.class, () -> {creditcardService.activateCard("404", false);});
    }

}
